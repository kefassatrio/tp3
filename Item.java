import java.util.ArrayList;
import java.util.List;

public class Item {

	// TODO tambahkan attribute-attribute lain yang diperlukan

	private String nama;
	private String deskripsi;
	private int usiaDari;
	private int usiaSampai;
	private String bahan;
	private List<Barang> barangList;

	public Item(String nama, String deskripsi, int usiaDari, int usiaSampai, String bahan) {
		this.nama = nama;
		this.deskripsi = deskripsi;
		this.usiaDari = usiaDari;
		this.usiaSampai = usiaSampai;
		this.bahan = bahan;
		this.barangList = new ArrayList<>();
	}

	// TODO tambahkan method-method yang diperlukan
	public void addBarang(Barang barang) {
		this.barangList.add(barang);
	}

	public String toString() {
		return nama;
	}

}
