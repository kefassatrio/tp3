import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Admin extends Pengguna implements Chatable {

	private List<Chat> chatList;

	public Admin(long nomorKTP, String namaLengkap, String email, LocalDate tanggalLahir, String nomorTelepon) {
		super(nomorKTP, namaLengkap, email, tanggalLahir, nomorTelepon);
		this.chatList = new ArrayList<>();
	}

	// TODO tambahkan method-method yang diperlukan
	public void chat(Pengguna receiver, String pesan) {
		Chat chat = new Chat(this, receiver, pesan, LocalDateTime.now());
		chatList.add(chat);
		System.out.println(
				"Admin " + this.getNamaLengkap() + " melakukan chat dengan anggota " + receiver.getNamaLengkap());
	}

	// buat nge chat anggota kalau barang unavailable
	public void chatBarangUnavailable(Anggota receiver, ArrayList<Barang> barangUnavailable) {
		String barangBarangUnavailable = "\n";
		for (Barang barang : barangUnavailable) {
			barangBarangUnavailable += "- " + barang + "\n";
		}
		String pesan = "\nBarang: " + barangBarangUnavailable
				+ " sedang tidak tersedia. Apakah anda ingin menunggu ketersediaan barang tersebut? Jika ya, untuk berapa lama?\n";
		Chat chat = new Chat(this, receiver, pesan, LocalDateTime.now());
		chatList.add(chat);
		System.out.println(chat);
	}

	public List<Chat> getChatList() {
		return chatList;
	}
}
