public class Alamat {

	private Anggota pemilik;
	private String namaAlamat;
	private String jalan;
	private long nomor;
	private String kota;
	private int kodePos;

	public Alamat(Anggota pemilik, String namaAlamat, String jalan, long nomor, String kota, int kodePos) {
		this.pemilik = pemilik;
		this.namaAlamat = namaAlamat;
		this.jalan = jalan;
		this.nomor = nomor;
		this.kota = kota;
		this.kodePos = kodePos;
	}

	// TODO tambahkan method-method yang diperlukan
	public String getAlamat() {
		return namaAlamat + ", " + jalan + ", " + nomor + ", " + kota + ", " + kodePos;
	}
}
