import java.util.ArrayList;
import java.util.List;

public class LevelKeanggotaan {

	// Tambahkan attribute-attribute yang diperlukan
	private String namaLevel;
	private long minimumPoin;
	private String deskripsi;

	private static List<LevelKeanggotaan> semuaLevelKeanggotaan = new ArrayList<>();

	public LevelKeanggotaan(String namaLevel, long minimumPoin, String deskripsi) {
		LevelKeanggotaan.semuaLevelKeanggotaan.add(this);
		this.namaLevel = namaLevel;
		this.minimumPoin = minimumPoin;
		this.deskripsi = deskripsi;
	}

	// TODO tambahkan method-method yang diperlukan
	public static List<LevelKeanggotaan> getSemuaLevelKeanggotaan() {
		return semuaLevelKeanggotaan;
	}

	public String getNamaLevel() {
		return namaLevel;
	}

	public long getMinimumPoin() {
		return minimumPoin;
	}
}
