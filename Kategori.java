import java.util.ArrayList;
import java.util.List;

public class Kategori {

	private String nama;
	private int level;
	private Kategori superKategori;
	private List<Kategori> subKategoriList;
	private List<Item> itemsList;

	// TODO tambahkan method-method yang diperlukan

	public Kategori(String nama) { // constructor buat bikin superkategori paling atas
		this.nama = nama;
		this.level = 1;
		this.subKategoriList = new ArrayList<>();
		this.itemsList = new ArrayList<>();
	}

	// constructor buat bikin sub kategori suatu superkategori
	public Kategori(Kategori superKategori, String nama) {
		this.superKategori = superKategori;
		this.nama = nama;
		this.level = superKategori.level + 1;
		this.subKategoriList = new ArrayList<>();
		this.itemsList = new ArrayList<>();
		superKategori.subKategoriList.add(this);
	}

	// add item ke sebuah kategori (super kategori atau sub kategori)
	public void addItem(Item item) {
		if (level == 1) { // kalo level 1 berarti udah paling atas
			this.itemsList.add(item);
		} else if (level > 1) { // kalo level lebih dari 1 akan add ke superkategorinya
			this.itemsList.add(item);
			this.addItemToSuperKategori(this.superKategori, item);
		}

	}

	// rekursi buat add item ke super kategori nya superkategori
	public void addItemToSuperKategori(Kategori superKategori, Item item) {
		if (superKategori.level == 1) { // kalo udah paling atas
			superKategori.itemsList.add(item);
		} else if (superKategori.level > 1) { // kalo belum akan add item ke superkategori nya superkategori
			superKategori.itemsList.add(item);
			addItemToSuperKategori(superKategori.superKategori, item); // superKategorinya superKategori
		}
	}

	public List<Item> getItemsList() {
		return itemsList;
	}

	public void getAllItems() { // nanti dipanggil di main
		System.out.println("Items dengan kategori " + this.nama + " :");
		for (Item item : itemsList) {
			System.out.println("- " + item);
		}
	}
}
