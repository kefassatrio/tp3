public class InfoBarangLevel {

	private Barang barang;
	private LevelKeanggotaan level;
	private long hargaSewa;
	private long porsiLoyalti;
	private int ongkos;

	public InfoBarangLevel(Barang barang, LevelKeanggotaan level, long hargaSewa, long porsiLoyalti) {
		this.barang = barang;
		this.level = level;
		this.hargaSewa = hargaSewa;
		this.porsiLoyalti = porsiLoyalti;
		barang.setInfoBarangLevel(this);
	}

	public void setOngkos(int ongkos) {
		this.ongkos = ongkos;
	}

	public long getHargaSewa() {
		return hargaSewa;
	}

	public long getOngkos() {
		return ongkos;
	}

	// TODO tambahkan method-method yang diperlukan
}
