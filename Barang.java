import java.util.ArrayList;

public class Barang {

	private Item item;
	private String warna;
	private String urlFoto;
	private String kondisi;
	private int lamaPenggunaan;
	private Anggota penyewa;
	private InfoBarangLevel infoBarangLevel;
	private Status status;
	private ArrayList<Anggota> akanMenyewaList;
	private ArrayList<Integer> lamaSewaList;
	private ArrayList<Integer> idPemesananSelanjutnyaList;
	private ArrayList<Admin> adminYangAkanMengurusSelanjutnya;

	public Barang(Item item, String warna, String urlFoto, String kondisi, int lamaPenggunaan) {
		this.item = item;
		this.warna = warna;
		this.urlFoto = urlFoto;
		this.kondisi = kondisi;
		this.lamaPenggunaan = lamaPenggunaan;
		this.status = new Status("Available");
		this.akanMenyewaList = new ArrayList<>();
		this.lamaSewaList = new ArrayList<>();
		this.idPemesananSelanjutnyaList = new ArrayList<>();
		this.adminYangAkanMengurusSelanjutnya = new ArrayList<>();

		item.addBarang(this);
	}

	public void setInfoBarangLevel(InfoBarangLevel infoBarangLevel) {
		this.infoBarangLevel = infoBarangLevel;
	}

	public void addAdminYangAkanMengurusSelanjutnya(Admin admin) {
		adminYangAkanMengurusSelanjutnya.add(admin);
	}

	public ArrayList<Admin> getAdminYangAkanMengurusSelanjutnya() {
		return adminYangAkanMengurusSelanjutnya;
	}

	public ArrayList<Anggota> getAkanMenyewaList() {
		return akanMenyewaList;
	}

	public void removeFirstIndexAkanMenyewaList() {
		this.akanMenyewaList.remove(0);
	}

	public void removeFirstIndexLamaSewaList() {
		this.lamaSewaList.remove(0);
	}

	public void removeFirstIndexIdPemesananSelanjutnyaList() {
		this.idPemesananSelanjutnyaList.remove(0);
	}

	public void removeFirstIndexAdminYangAkanMengurusSelanjutnya() {
		this.adminYangAkanMengurusSelanjutnya.remove(0);
	}

	public void addAkanMenyewaList(Anggota anggota) {
		this.akanMenyewaList.add(anggota);
	}

	public void addIdPemesananSelanjutnya(Integer id) {
		this.idPemesananSelanjutnyaList.add(id);
	}

	public ArrayList<Integer> getLamaSewaList() {
		return lamaSewaList;
	}

	public ArrayList<Integer> getIdPemesananSelanjutnyaList() {
		return idPemesananSelanjutnyaList;
	}

	public void addLamaSewaList(Integer lamaSewa) {
		lamaSewaList.add(lamaSewa);
	}

	public InfoBarangLevel getInfoBarangLevel() {
		return infoBarangLevel;
	}

	public void setStatus(String status) {
		this.status.setStatus(status);
	}

	public String getBarangStatus() {
		return status.getStatus();
	}

	public String toString() { // to string nya nama item dan warnanya
		return item + " " + warna;
	}
}
