import java.time.LocalDateTime;
import java.util.ArrayList;

public class Pemesanan implements Orderable {

	// TODO tambahkan attribute-attribute yang diperlukan

	private int id;
	private LocalDateTime dateTimePesanan;
	private int kuantitasBarang;
	private long hargaSewa;
	private long ongkos;
	private Anggota pemesan;
	private Status status;
	private ArrayList<Barang> barangList = new ArrayList<>();

	// multiple barang
	public Pemesanan(int id, ArrayList<Barang> barangList, LocalDateTime dateTimePesanan, int kuantitasBarang,
			long hargaSewa, long ongkos, Anggota pemesan, Status status) {
		this.id = id;
		this.barangList = barangList;
		this.dateTimePesanan = dateTimePesanan;
		this.kuantitasBarang = kuantitasBarang;
		this.hargaSewa = hargaSewa;
		this.ongkos = ongkos;
		this.pemesan = pemesan;
		this.status = status;
	}

	// TODO tambahkan method-method yang diperlukan
	public long getOngkos() {
		return ongkos;
	}

	public int getId() {
		return id;
	}

	public ArrayList<Barang> getBarangList() {
		return barangList;
	}

	public LocalDateTime getDateTimePesanan() {
		return dateTimePesanan;
	}

	public Anggota getPemesan() {
		return pemesan;
	}

	public int getKuantitasBarang() {
		return kuantitasBarang;
	}

	public long getHargaSewa() {
		return hargaSewa;
	}

	public Status getPemesananStatus() {
		return status;
	}

	public String toString() {
		return "PEMESANAN [" + LocalDateTime.now() + "] - ID: " + id + " - Kuantitas: " + kuantitasBarang
				+ " - Harga Sewa: " + hargaSewa + " - Ongkos: " + ongkos + " - Status: " + status.getStatus();
	}
}
