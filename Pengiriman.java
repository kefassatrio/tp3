import java.time.LocalDate;

public class Pengiriman implements Orderable {

	// TODO tambahkan attribute-attribute yang diperlukan
	private long resi;
	private Pemesanan pemesanan;
	private String metode;
	private long ongkos;
	private LocalDate datePengiriman;
	private Alamat alamat;

	public Pengiriman(long resi, Pemesanan pemesanan, String metode, long ongkos, LocalDate datePengiriman,
			Alamat alamat) {
		this.resi = resi;
		this.pemesanan = pemesanan;
		this.metode = metode;
		this.ongkos = ongkos;
		this.datePengiriman = datePengiriman;
		this.alamat = alamat;
	}

	// TODO tambahkan method-method yang diperlukan
	public long getOngkos() {
		return ongkos;
	}

	public String toString() {
		return "PENGIRIMAN [" + datePengiriman + "] - Nomor Resi: " + resi + " - Metode: " + metode + " - Pemesan: "
				+ pemesanan.getPemesan().getNamaLengkap() + " - Alamat: [" + alamat.getAlamat() + "]";
	}

}
