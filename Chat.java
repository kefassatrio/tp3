import java.time.LocalDateTime;

public class Chat {

	private String pesan;
	private LocalDateTime dateTime;
	private Pengguna sender;
	private Pengguna receiver;

	// TODO tambahkan method-method yang diperlukan
	public Chat(Pengguna sender, Pengguna receiver, String pesan, LocalDateTime dateTime) {
		this.sender = sender;
		this.receiver = receiver;
		this.pesan = pesan;
		this.dateTime = dateTime;
	}

	public String toString() {
		return "From: " + sender.getNamaLengkap() + " - To: " + receiver.getNamaLengkap() + ", Pesan: " + pesan;
	}
}
