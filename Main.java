import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		LevelKeanggotaan levelGold = new LevelKeanggotaan("Gold", 1000, "Gold level");
		LevelKeanggotaan levelSilver = new LevelKeanggotaan("Silver", 500, "Silver level");
		LevelKeanggotaan levelBronze = new LevelKeanggotaan("Bronze", 0, "Bronze level");

		ArrayList<Barang> barangList = new ArrayList<>();
		ArrayList<InfoBarangLevel> infoBarangLevelList = new ArrayList<>();
		ArrayList<Barang> barangList2 = new ArrayList<>();
		ArrayList<InfoBarangLevel> infoBarangLevelList2 = new ArrayList<>();
		ArrayList<Barang> barangList3 = new ArrayList<>();
		ArrayList<InfoBarangLevel> infoBarangLevelList3 = new ArrayList<>();
		// Pengguna penggunaBruce = new Pengguna(123, "Bruce",
		// "bruce@mail.com", LocalDate.of(1999, 8, 1),
		// "+621919912");
		// Akan error jika di-uncomment. Gunakan fakta tersebut untuk membuat class atau
		// interface yang tepat.

		Admin adminJoe = new Admin(123, "Joe", "joe@mail.com", LocalDate.of(1999, 8, 1), "+62191991");
		Anggota anggotaSteve = new Anggota(444, "Steve", "steve@cap.mail", LocalDate.of(2003, 9, 1), "+62185938", 1000,
				levelGold);

		Anggota anggotaKhitil = new Anggota(444, "Khitil", "Khitil@cap.mail", LocalDate.of(2003, 9, 1), "+62185938",
				1000, levelGold);
		Alamat alamatKhitil = new Alamat(anggotaKhitil, "Khitil City", "Hope St.", 99, "NYC", 240419);
		anggotaKhitil.getAlamatList().add(alamatKhitil);

		Anggota anggotaBronze = new Anggota(123, "bron", "bron@cap.mail", LocalDate.of(2003, 9, 1), "+62185938", 499,
				levelBronze);

		// Tes fungsionalitas chat
		anggotaSteve.chat(adminJoe, "Salam kenal, min");
		// SHOULD PRINT "Anggota Steve melakukan chat dengan admin Joe"
		anggotaSteve.chat(adminJoe, "Apa kabar?");
		// SHOULD PRINT "Anggota Steve melakukan chat dengan admin Joe"
		for (Chat chat : anggotaSteve.getChatList()) {
			System.out.println(chat);
		}
		/*
		 * SHOULD PRINT THESE: *REVISED*
		 * "From: Steve - To: Joe, Pesan: Salam kenal, min"
		 * "From: Steve - To: Joe, Pesan: Apa kabar?"
		 */
		Item blindFold = new Item("blindFold", "blindfold", 1, 22, "kain");
		Barang blindFoldKuning = new Barang(blindFold, "kuning", "sdasdasd", "no", 12);
		InfoBarangLevel blindFoldKuningInfoBarangLevel = new InfoBarangLevel(blindFoldKuning, levelBronze, 122, 10);
		barangList3.add(blindFoldKuning);
		infoBarangLevelList3.add(blindFoldKuningInfoBarangLevel);

		// cek pergantian level keanggotaan anggotaBronze
		System.out.println(anggotaBronze.getLevelKeanggotaan().getNamaLevel());
		anggotaBronze.doOrder(adminJoe, 1012, barangList3, 4, infoBarangLevelList3);
		System.out.println(anggotaBronze.getLevelKeanggotaan().getNamaLevel());

		Item topi = new Item("Topi", "A hat", 5, 12, "Katun premium");
		Barang topiMerah = new Barang(topi, "Merah", "http://example.com/hat.png", "Nice", 1);
		Barang topiHitam = new Barang(topi, "Hitam", "http://example.com/hat.png", "Gud", 1);
		barangList.add(topiMerah);
		barangList.add(topiHitam);

		Item kepalakepala = new Item("kepalakepala", "kepala", 1, 23, "katun");
		Barang palapala = new Barang(kepalakepala, "hitam", "ssss", "rusak", 122);

		barangList2.add(palapala);

		InfoBarangLevel infoPalaPala = new InfoBarangLevel(palapala, levelSilver, 4000, 10);
		infoPalaPala.setOngkos(1000);
		infoBarangLevelList2.add(infoPalaPala);

		// CEK REKURSI KATEGORI

		Kategori fashion = new Kategori("Fashion");
		Kategori kepala = new Kategori(fashion, "Kepala");
		fashion.addItem(topi);
		Item headband = new Item("Headband", "headband", 1, 20, "katun");
		Item kacamata = new Item("Kacamata", "kacamata", 1, 12, "kaca");
		kepala.addItem(headband);
		kepala.addItem(kacamata);
		kepala.addItem(kepalakepala);
		fashion.getAllItems();
		kepala.getAllItems();

		InfoBarangLevel infoTopiMerah = new InfoBarangLevel(topiMerah, levelGold, 3000, 10);
		// 3000 menandakan harga sewa per pekan (mengikuti contoh pada deskripsi
		// sistem), 10 menandakan persentase loyalti
		infoTopiMerah.setOngkos(10000);
		InfoBarangLevel infoTopiHitam = new InfoBarangLevel(topiHitam, levelGold, 4000, 15);
		infoTopiHitam.setOngkos(10000);
		infoBarangLevelList.add(infoTopiMerah);
		infoBarangLevelList.add(infoTopiHitam);

		// Tes fungsionalitas pemesanan barang
		anggotaSteve.doOrder(adminJoe, 100, barangList, 3, infoBarangLevelList);
		anggotaSteve.doOrder(adminJoe, 102, barangList2, 3, infoBarangLevelList2);

		Pemesanan pemesananSteve2 = (Pemesanan) anggotaSteve.getPemesananList().get(0);
		Alamat alamatSteve2 = new Alamat(anggotaSteve, "Khitil City", "Hope St.", 99, "NYC", 240419);
		anggotaSteve.sendPemesanan(9991, pemesananSteve2, "Metode cepat", pemesananSteve2.getOngkos(), LocalDate.now(),
				alamatSteve2);
		// cek bisa queue atau ga
		anggotaKhitil.doOrder(adminJoe, 111, barangList, 12, infoBarangLevelList2);
		// nanti adminJoe otomatis ngasih chat kalo ada barang tidak tersedia
		anggotaKhitil.mauMenungguBarang(adminJoe, anggotaKhitil.getBarangUnavailableYgMungkinMau(), 9999, 19);
		anggotaSteve.doReturn(999, pemesananSteve2, "Metode cepat", pemesananSteve2.getOngkos(), LocalDate.now(),
				alamatSteve2);
		anggotaKhitil.seeOrders();

		// 3 menandakan lama sewa
		// SHOULD PRINT "Anggota Steve memesan barang"
		anggotaSteve.seeOrders();
		// SHOULD PRINT "Pemesanan dengan ID[100] dipesan oleh anggota Steve"
		// SHOULD PRINT "PEMESANAN [2019-04-26T00:21:36.014346800] - Kuantitas: 2 -
		// Harga Sewa: 21000 - Ongkos: 20000 - Status: Dipesan"

		Chatable anggotaScott = new Anggota(443, "Scott", "scott@ant.mail", LocalDate.of(2002, 9, 1), "+6218885938",
				100, levelGold);
		// Chatable chatable = new Chatable();
		// Akan error jika di-uncomment. Gunakan fakta tersebut untuk membuat class atau
		// interface yang tepat.

		((Anggota) anggotaScott).seeOrders();
		// SHOULD PRINT "Anggota Scott belum memesan apapun"

		Alamat alamatSteve = new Alamat(anggotaSteve, "Brooklyn", "Hope St.", 99, "NYC", 240419);
		anggotaSteve.getAlamatList().add(alamatSteve);

		Pemesanan pemesananSteve = (Pemesanan) anggotaSteve.getPemesananList().get(0);

		// Tes fungsionalitas pengiriman
		anggotaSteve.sendPemesanan(9991, pemesananSteve, "Metode cepat", pemesananSteve.getOngkos(), LocalDate.now(),
				alamatSteve);
		// SHOULD PRINT "Anggota Steve mengirim barang"
		// SHOULD PRINT "Pengiriman dengan nomor resi [9991] dikirim oleh anggota Steve

		// Tes fungsionalitas pengembalian
		anggotaSteve.doReturn(9992, pemesananSteve, "Metode cepat", pemesananSteve.getOngkos(), LocalDate.now(),
				alamatSteve);
		// SHOULD PRINT "Anggota Steve mengembalikan barang
		// SHOULD PRINT "Pengembalian dengan nomor resi [9992] dikembalikan oleh anggota
		// Steve

		anggotaSteve.seeOrders();
		// SHOULD PRINT "PEMESANAN [2019-04-26T11:10:05.329213600] - ID: 100 -
		// Kuantitas: 2 - Harga Sewa: 21000 - Ongkos: 20000 - Status: Dipesan"
		// SHOULD PRINT "PENGIRIMAN [2019-04-26] - Nomor Resi: 9991 - Metode: Metode
		// cepat - Pemesan: Steve - Alamat: [Brooklyn, Hope St., 99, NYC, 240419]"
		// SHOULD PRINT "PENGEMBALIAN [2019-04-26] - Nomor Resi: 9992 - Metode: Metode
		// cepat - Pemesan: Steve - Alamat: [Brooklyn, Hope St., 99, NYC, 240419]"

		anggotaKhitil.seeOrders();
	}
}
