import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Anggota extends Pengguna implements Chatable {

	// TODO tambahkan attribute-attribute yang diperlukan
	private long poin;
	private LevelKeanggotaan levelKeanggotaan;
	private List<Chat> chatList;
	private List<Alamat> alamatList;
	private List<Pemesanan> pemesananList;
	private List<Orderable> orderList;
	private List<Barang> inventory;
	private ArrayList<Barang> barangUnvailableYgMungkinMau;

	// constructor
	public Anggota(long nomorKTP, String namaLengkap, String email, LocalDate tanggalLahir, String nomorTelepon,
			long poin, LevelKeanggotaan levelKeanggotaan) {
		super(nomorKTP, namaLengkap, email, tanggalLahir, nomorTelepon);
		this.poin = poin;
		this.levelKeanggotaan = levelKeanggotaan;
		this.chatList = new ArrayList<>();
		this.alamatList = new ArrayList<>();
		this.pemesananList = new ArrayList<>();
		this.orderList = new ArrayList<>();
		this.inventory = new ArrayList<>();
		this.barangUnvailableYgMungkinMau = new ArrayList<>();
	}

	// TODO tambahkan method-method yang diperlukan
	public long getPoin() {
		return poin;
	}

	public void addPemesananList(Pemesanan pemesanan) {
		this.pemesananList.add(pemesanan);
	}

	public void addOrderList(Orderable order) {
		this.orderList.add(order);
	}

	public void addBarangToInventory(Barang barang) {
		inventory.add(barang);
	}

	// buat nge chat ke sesama pengguna
	public void chat(Pengguna receiver, String pesan) {
		Chat chat = new Chat(this, receiver, pesan, LocalDateTime.now());
		chatList.add(chat);
		System.out.println(
				"Anggota " + this.getNamaLengkap() + " melakukan chat dengan admin " + receiver.getNamaLengkap());
	}

	public List<Chat> getChatList() {
		return chatList;
	}

	public List<Alamat> getAlamatList() {
		return alamatList;
	}

	public ArrayList<Barang> getBarangUnavailableYgMungkinMau() {
		return barangUnvailableYgMungkinMau;
	}

	public void removeFirstIndexBarangUnavailableYgMungkinMau() {
		barangUnvailableYgMungkinMau.remove(0);
	}

	// memesan barang
	public void doOrder(Admin admin, int id, ArrayList<Barang> barangList, int lamaSewa,
			ArrayList<InfoBarangLevel> infoBarangLevelList) { // parameter admin buat mesen ke admin siapa

		ArrayList<Barang> barangAvailable = new ArrayList<>();
		ArrayList<InfoBarangLevel> infoBarangLevelAvailable = new ArrayList<>();

		ArrayList<Barang> barangUnavailable = new ArrayList<>();
		// kalau available dimasukin ke arraylist barang available
		for (Barang barang : barangList) {
			if (barang.getBarangStatus().equals("Available")) {
				barangAvailable.add(barang);
				infoBarangLevelAvailable.add(barang.getInfoBarangLevel());
			} else {
				barangUnavailable.add(barang);
			}
		}

		if (barangAvailable.size() > 0) {
			// nambah poin
			this.poin += 10;
			// cek naik level atau tidak
			for (LevelKeanggotaan levelKeanggotaanLain : LevelKeanggotaan.getSemuaLevelKeanggotaan()) {
				if (this.poin >= levelKeanggotaanLain.getMinimumPoin()
						&& this.levelKeanggotaan.getMinimumPoin() <= levelKeanggotaanLain.getMinimumPoin()) {
					this.levelKeanggotaan = levelKeanggotaanLain;
				}
			}

			Anggota pemesan = this;
			Status status = new Status("Dipesan");
			int kuantitasBarang = barangAvailable.size();
			long totalOngkos = 0;
			long hargaSewa = 0;
			// hitung total ongkos dan harga sewa
			for (InfoBarangLevel infoBarang : infoBarangLevelAvailable) {
				totalOngkos += infoBarang.getOngkos();
				hargaSewa += lamaSewa * infoBarang.getHargaSewa();
			}
			// mengganti status available barang barang
			for (Barang barang : barangAvailable) {
				barang.setStatus("Unavailable");
			}

			Pemesanan pemesanan = new Pemesanan(id, barangAvailable, LocalDateTime.now(), kuantitasBarang, hargaSewa,
					totalOngkos, pemesan, status);

			this.addPemesananList(pemesanan);

			this.addOrderList(pemesanan);

			System.out.println("Pemesanan dengan ID[" + pemesanan.getId() + "] dipesan oleh anggota "
					+ pemesanan.getPemesan().getNamaLengkap());
		}

		// kalau ada barang yang ga available
		if (barangUnavailable.size() > 0) {
			barangUnvailableYgMungkinMau = barangUnavailable;
			admin.chatBarangUnavailable(this, barangUnavailable);

		}

	}

	public void mauMenungguBarang(Admin receiver, ArrayList<Barang> barangUnavailable, int id, int lamaSewa) {
		for (Barang barang : barangUnavailable) {
			barang.addAkanMenyewaList(this);
			barang.addLamaSewaList(lamaSewa);
			barang.addIdPemesananSelanjutnya(id);
			barang.addAdminYangAkanMengurusSelanjutnya(receiver);
		}
		// hilangin barang barang yang ada di arraylist barangUnavailableYgMungkinMau
		for (int i = 0; i < barangUnavailable.size(); i++) {
			removeFirstIndexBarangUnavailableYgMungkinMau();
		}
		Chat chat = new Chat(this, receiver,
				"Ya saya ingin menunggu. " + "Saya ingin menyewa selama " + lamaSewa + " hari.", LocalDateTime.now());
		chatList.add(chat);
		System.out.println(chat);
	}

	public void seeOrders() {
		if (this.orderList.size() > 0) {
			for (Orderable order : this.orderList) {
				System.out.println(order);
			}
		} else
			System.out.println("Anggota " + namaLengkap + " belum memesan apapun");
	}

	public List<Pemesanan> getPemesananList() {
		return pemesananList;
	}

	public void sendPemesanan(long resi, Pemesanan pemesanan, String metode, long ongkos, LocalDate datePengiriman,
			Alamat alamat) {

		Pengiriman pengiriman = new Pengiriman(resi, pemesanan, metode, ongkos, datePengiriman, alamat);

		this.orderList.add(pengiriman);

		pemesanan.getPemesananStatus().setStatus("Delivered");
		System.out.println(
				"Pengiriman pemesanan " + namaLengkap + " dengan nomor resi [" + resi + "] telah dikirim oleh admin");
	}

	public void doReturn(long resi, Pemesanan pemesanan, String metode, long ongkos, LocalDate datePengembalian,
			Alamat alamat) {
		Pengembalian pengembalian = new Pengembalian(resi, pemesanan, metode, ongkos, datePengembalian, alamat);

		orderList.add(pengembalian);

		pemesanan.getPemesananStatus().setStatus("Returned");
		System.out.println("Pengembalian dengan nomor resi [" + resi + "] dikembalikan oleh anggota " + namaLengkap);

		for (Barang barang : pemesanan.getBarangList()) {
			barang.setStatus("Available");
			if (barang.getAkanMenyewaList().size() > 0) {
				// cek kalo barang yang lagi di iterate ada yang udah mesen atau ga
				// kalo ada langsung otomatis bikinin pemesanan dan langsung send pemesanan nya
				ArrayList<Barang> barangListSelanjutnya = new ArrayList<>();
				barangListSelanjutnya.add(barang);
				ArrayList<InfoBarangLevel> infoBarangLevelSelanjutnya = new ArrayList<>();
				infoBarangLevelSelanjutnya.add(barang.getInfoBarangLevel());
				barang.getAkanMenyewaList().get(0).doOrder(barang.getAdminYangAkanMengurusSelanjutnya().get(0),
						barang.getIdPemesananSelanjutnyaList().get(0), barangListSelanjutnya,
						barang.getLamaSewaList().get(0), infoBarangLevelSelanjutnya);

				barang.getAkanMenyewaList().get(0).sendPemesanan(new Random().nextInt(1000),
						barang.getAkanMenyewaList().get(0).getPemesananList().get(0), "Metode cepat",
						barang.getAkanMenyewaList().get(0).getPemesananList().get(0).getOngkos(), LocalDate.now(),
						barang.getAkanMenyewaList().get(0).getAlamatList().get(0));

				barang.removeFirstIndexAkanMenyewaList();
				barang.removeFirstIndexAdminYangAkanMengurusSelanjutnya();
				barang.removeFirstIndexIdPemesananSelanjutnyaList();
				barang.removeFirstIndexLamaSewaList();
			}

		}
	}

	public void Menyewakan(ArrayList<Barang> gudang, Barang barang) {
		if (this.inventory.contains(barang)) {
			// nambah poin
			this.poin += 10;
			// cek naik level atau tidak
			for (LevelKeanggotaan levelKeanggotaanLain : LevelKeanggotaan.getSemuaLevelKeanggotaan()) {
				if (this.poin >= levelKeanggotaanLain.getMinimumPoin()
						&& this.levelKeanggotaan.getMinimumPoin() <= levelKeanggotaanLain.getMinimumPoin()) {
					this.levelKeanggotaan = levelKeanggotaanLain;
				}
			}

			inventory.remove(barang);
			gudang.add(barang);

		} else {
			System.out.format("Barang tidak ada di inventory %s", namaLengkap);
		}
	}

	public LevelKeanggotaan getLevelKeanggotaan() {
		return this.levelKeanggotaan;
	}
}
