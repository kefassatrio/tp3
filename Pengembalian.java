import java.time.LocalDate;

public class Pengembalian implements Orderable {
	private long resi;
	private Pemesanan pemesanan;
	private String metode;
	private long ongkos;
	private LocalDate datePengembalian;
	private Alamat alamat;

	public Pengembalian(long resi, Pemesanan pemesanan, String metode, long ongkos, LocalDate datePengembalian,
			Alamat alamat) {
		this.resi = resi;
		this.pemesanan = pemesanan;
		this.metode = metode;
		this.ongkos = ongkos;
		this.datePengembalian = datePengembalian;
		this.alamat = alamat;
	}

	public long getOngkos() {
		return ongkos;
	}

	public String toString() {
		return "PENGEMBALIAN [" + datePengembalian + "] - Nomor Resi: " + resi + " - Metode: " + metode + " - Pemesan: "
				+ pemesanan.getPemesan().getNamaLengkap() + " - Alamat: [" + alamat.getAlamat() + "]";
	}

}
