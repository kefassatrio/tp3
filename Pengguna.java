import java.time.LocalDate;

public abstract class Pengguna {

	// TODO tambahkan attribute-attribute yang diperlukan
	protected long nomorKTP;
	protected String namaLengkap;
	protected String email;
	protected LocalDate tanggalLahir;
	protected String nomorTelepon;

	public Pengguna(long nomorKTP, String namaLengkap, String email, LocalDate tanggalLahir, String nomorTelepon) {
		this.nomorKTP = nomorKTP;
		this.namaLengkap = namaLengkap;
		this.email = email;
		this.tanggalLahir = tanggalLahir;
		this.nomorTelepon = nomorTelepon;
	}

	// TODO tambahkan method-method yang diperlukan
	public long getNomorKTP() {
		return nomorKTP;
	}

	public String getNamaLengkap() {
		return namaLengkap;
	}

	public String getEmail() {
		return email;
	}

	public LocalDate getTanggalLahir() {
		return tanggalLahir;
	}

	public String getNomorTelepon() {
		return nomorTelepon;
	}
}
